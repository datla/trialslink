package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Characteristic;
import com.trialslink.domain.enumeration.Sex;
import com.trialslink.repository.CharacteristicRepository;
import com.trialslink.service.CharacteristicService;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the CharacteristicResource REST controller.
 *
 * @see CharacteristicResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class CharacteristicResourceIntTest {

    private static final String DEFAULT_CRITERIA = "AAAAAAAAAA";
    private static final String UPDATED_CRITERIA = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Sex DEFAULT_SEX = Sex.MALE;
    private static final Sex UPDATED_SEX = Sex.FEMALE;

    private static final Boolean DEFAULT_BASED_ON_GENDER = false;
    private static final Boolean UPDATED_BASED_ON_GENDER = true;

    private static final Boolean DEFAULT_ACCEPT_HEALTHY_VOLUNTEERS = false;
    private static final Boolean UPDATED_ACCEPT_HEALTHY_VOLUNTEERS = true;

    @Autowired
    private CharacteristicRepository characteristicRepository;

    @Autowired
    private CharacteristicService characteristicService;
    @Autowired
    private TrialService trialService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restCharacteristicMockMvc;

    private Characteristic characteristic;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CharacteristicResource characteristicResource = new CharacteristicResource(characteristicService, trialService);
        this.restCharacteristicMockMvc = MockMvcBuilders.standaloneSetup(characteristicResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Characteristic createEntity() {
        Characteristic characteristic = new Characteristic()
            .criteria(DEFAULT_CRITERIA)
            .code(DEFAULT_CODE)
            .sex(DEFAULT_SEX)
            .basedOnGender(DEFAULT_BASED_ON_GENDER)
            .acceptHealthyVolunteers(DEFAULT_ACCEPT_HEALTHY_VOLUNTEERS);
        return characteristic;
    }

    @Before
    public void initTest() {
        characteristicRepository.deleteAll();
        characteristic = createEntity();
    }

    @Test
    public void createCharacteristic() throws Exception {
        int databaseSizeBeforeCreate = characteristicRepository.findAll().size();

        // Create the Characteristic
        restCharacteristicMockMvc.perform(post("/api/characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(characteristic)))
            .andExpect(status().isCreated());

        // Validate the Characteristic in the database
        List<Characteristic> characteristicList = characteristicRepository.findAll();
        assertThat(characteristicList).hasSize(databaseSizeBeforeCreate + 1);
        Characteristic testCharacteristic = characteristicList.get(characteristicList.size() - 1);
        assertThat(testCharacteristic.getCriteria()).isEqualTo(DEFAULT_CRITERIA);
        assertThat(testCharacteristic.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCharacteristic.getSex()).isEqualTo(DEFAULT_SEX);
        assertThat(testCharacteristic.isBasedOnGender()).isEqualTo(DEFAULT_BASED_ON_GENDER);
        assertThat(testCharacteristic.isAcceptHealthyVolunteers()).isEqualTo(DEFAULT_ACCEPT_HEALTHY_VOLUNTEERS);
    }

    @Test
    public void createCharacteristicWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = characteristicRepository.findAll().size();

        // Create the Characteristic with an existing ID
        characteristic.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCharacteristicMockMvc.perform(post("/api/characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(characteristic)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Characteristic> characteristicList = characteristicRepository.findAll();
        assertThat(characteristicList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkCriteriaIsRequired() throws Exception {
        int databaseSizeBeforeTest = characteristicRepository.findAll().size();
        // set the field null
        characteristic.setCriteria(null);

        // Create the Characteristic, which fails.

        restCharacteristicMockMvc.perform(post("/api/characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(characteristic)))
            .andExpect(status().isBadRequest());

        List<Characteristic> characteristicList = characteristicRepository.findAll();
        assertThat(characteristicList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkSexIsRequired() throws Exception {
        int databaseSizeBeforeTest = characteristicRepository.findAll().size();
        // set the field null
        characteristic.setSex(null);

        // Create the Characteristic, which fails.

        restCharacteristicMockMvc.perform(post("/api/characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(characteristic)))
            .andExpect(status().isBadRequest());

        List<Characteristic> characteristicList = characteristicRepository.findAll();
        assertThat(characteristicList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkAcceptHealthyVolunteersIsNotRequired() throws Exception {
        int databaseSizeBeforeTest = characteristicRepository.findAll().size();
        // set the field null
        characteristic.setAcceptHealthyVolunteers(null);

        // Create the Characteristic, which should be okay.
        restCharacteristicMockMvc.perform(post("/api/characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(characteristic)))
            .andExpect(status().isCreated());

        List<Characteristic> characteristicList = characteristicRepository.findAll();
        assertThat(characteristicList).hasSize(databaseSizeBeforeTest + 1);
    }

    @Test
    public void getAllCharacteristics() throws Exception {
        // Initialize the database
        characteristicRepository.save(characteristic);

        // Get all the characteristicList
        restCharacteristicMockMvc.perform(get("/api/characteristics?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(characteristic.getId())))
            .andExpect(jsonPath("$.[*].criteria").value(hasItem(DEFAULT_CRITERIA.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].sex").value(hasItem(DEFAULT_SEX.toString())))
            .andExpect(jsonPath("$.[*].basedOnGender").value(hasItem(DEFAULT_BASED_ON_GENDER.booleanValue())))
            .andExpect(jsonPath("$.[*].acceptHealthyVolunteers").value(hasItem(DEFAULT_ACCEPT_HEALTHY_VOLUNTEERS.booleanValue())));
    }

    @Test
    public void getCharacteristic() throws Exception {
        // Initialize the database
        characteristicRepository.save(characteristic);

        // Get the characteristic
        restCharacteristicMockMvc.perform(get("/api/characteristics/{id}", characteristic.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(characteristic.getId()))
            .andExpect(jsonPath("$.criteria").value(DEFAULT_CRITERIA.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.sex").value(DEFAULT_SEX.toString()))
            .andExpect(jsonPath("$.basedOnGender").value(DEFAULT_BASED_ON_GENDER.booleanValue()))
            .andExpect(jsonPath("$.acceptHealthyVolunteers").value(DEFAULT_ACCEPT_HEALTHY_VOLUNTEERS.booleanValue()));
    }

    @Test
    public void getNonExistingCharacteristic() throws Exception {
        // Get the characteristic
        restCharacteristicMockMvc.perform(get("/api/characteristics/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCharacteristic() throws Exception {
        // Initialize the database
        characteristicService.save(characteristic);

        int databaseSizeBeforeUpdate = characteristicRepository.findAll().size();

        // Update the characteristic
        Characteristic updatedCharacteristic = characteristicRepository.findOne(characteristic.getId());
        updatedCharacteristic
            .criteria(UPDATED_CRITERIA)
            .code(UPDATED_CODE)
            .sex(UPDATED_SEX)
            .basedOnGender(UPDATED_BASED_ON_GENDER)
            .acceptHealthyVolunteers(UPDATED_ACCEPT_HEALTHY_VOLUNTEERS);

        restCharacteristicMockMvc.perform(put("/api/characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCharacteristic)))
            .andExpect(status().isOk());

        // Validate the Characteristic in the database
        List<Characteristic> characteristicList = characteristicRepository.findAll();
        assertThat(characteristicList).hasSize(databaseSizeBeforeUpdate);
        Characteristic testCharacteristic = characteristicList.get(characteristicList.size() - 1);
        assertThat(testCharacteristic.getCriteria()).isEqualTo(UPDATED_CRITERIA);
        assertThat(testCharacteristic.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCharacteristic.getSex()).isEqualTo(UPDATED_SEX);
        assertThat(testCharacteristic.isBasedOnGender()).isEqualTo(UPDATED_BASED_ON_GENDER);
        assertThat(testCharacteristic.isAcceptHealthyVolunteers()).isEqualTo(UPDATED_ACCEPT_HEALTHY_VOLUNTEERS);
    }

    @Test
    public void updateNonExistingCharacteristic() throws Exception {
        int databaseSizeBeforeUpdate = characteristicRepository.findAll().size();

        // Create the Characteristic

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCharacteristicMockMvc.perform(put("/api/characteristics")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(characteristic)))
            .andExpect(status().isCreated());

        // Validate the Characteristic in the database
        List<Characteristic> characteristicList = characteristicRepository.findAll();
        assertThat(characteristicList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteCharacteristic() throws Exception {
        // Initialize the database
        characteristicService.save(characteristic);

        int databaseSizeBeforeDelete = characteristicRepository.findAll().size();

        // Get the characteristic
        restCharacteristicMockMvc.perform(delete("/api/characteristics/{id}", characteristic.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Characteristic> characteristicList = characteristicRepository.findAll();
        assertThat(characteristicList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Characteristic.class);
    }
}
