package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;

import com.trialslink.domain.Intervention;
import com.trialslink.repository.InterventionRepository;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InterventionResource REST controller.
 *
 * @see InterventionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class InterventionResourceIntTest {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_SYSTEM = "AAAAAAAAAA";
    private static final String UPDATED_SYSTEM = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private InterventionRepository interventionRepository;
    @Autowired
    private TrialService trialService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restInterventionMockMvc;

    private Intervention intervention;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InterventionResource interventionResource = new InterventionResource(interventionRepository, trialService);
        this.restInterventionMockMvc = MockMvcBuilders.standaloneSetup(interventionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Intervention createEntity() {
        Intervention intervention = new Intervention()
            .label(DEFAULT_LABEL)
            .system(DEFAULT_SYSTEM)
            .code(DEFAULT_CODE)
            .type(DEFAULT_TYPE);
        return intervention;
    }

    @Before
    public void initTest() {
        interventionRepository.deleteAll();
        intervention = createEntity();
    }

    @Test
    public void createIntervention() throws Exception {
        int databaseSizeBeforeCreate = interventionRepository.findAll().size();

        // Create the Intervention
        restInterventionMockMvc.perform(post("/api/interventions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(intervention)))
            .andExpect(status().isCreated());

        // Validate the Intervention in the database
        List<Intervention> interventionList = interventionRepository.findAll();
        assertThat(interventionList).hasSize(databaseSizeBeforeCreate + 1);
        Intervention testIntervention = interventionList.get(interventionList.size() - 1);
        assertThat(testIntervention.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testIntervention.getSystem()).isEqualTo(DEFAULT_SYSTEM);
        assertThat(testIntervention.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testIntervention.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    public void createInterventionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = interventionRepository.findAll().size();

        // Create the Intervention with an existing ID
        intervention.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restInterventionMockMvc.perform(post("/api/interventions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(intervention)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Intervention> interventionList = interventionRepository.findAll();
        assertThat(interventionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = interventionRepository.findAll().size();
        // set the field null
        intervention.setLabel(null);

        // Create the Intervention, which fails.

        restInterventionMockMvc.perform(post("/api/interventions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(intervention)))
            .andExpect(status().isBadRequest());

        List<Intervention> interventionList = interventionRepository.findAll();
        assertThat(interventionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllInterventions() throws Exception {
        // Initialize the database
        interventionRepository.save(intervention);

        // Get all the interventionList
        restInterventionMockMvc.perform(get("/api/interventions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(intervention.getId())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL.toString())))
            .andExpect(jsonPath("$.[*].system").value(hasItem(DEFAULT_SYSTEM.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    public void getIntervention() throws Exception {
        // Initialize the database
        interventionRepository.save(intervention);

        // Get the intervention
        restInterventionMockMvc.perform(get("/api/interventions/{id}", intervention.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(intervention.getId()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL.toString()))
            .andExpect(jsonPath("$.system").value(DEFAULT_SYSTEM.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    public void getNonExistingIntervention() throws Exception {
        // Get the intervention
        restInterventionMockMvc.perform(get("/api/interventions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateIntervention() throws Exception {
        // Initialize the database
        interventionRepository.save(intervention);
        int databaseSizeBeforeUpdate = interventionRepository.findAll().size();

        // Update the intervention
        Intervention updatedIntervention = interventionRepository.findOne(intervention.getId());
        updatedIntervention
            .label(UPDATED_LABEL)
            .system(UPDATED_SYSTEM)
            .code(UPDATED_CODE)
            .type(UPDATED_TYPE);

        restInterventionMockMvc.perform(put("/api/interventions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIntervention)))
            .andExpect(status().isOk());

        // Validate the Intervention in the database
        List<Intervention> interventionList = interventionRepository.findAll();
        assertThat(interventionList).hasSize(databaseSizeBeforeUpdate);
        Intervention testIntervention = interventionList.get(interventionList.size() - 1);
        assertThat(testIntervention.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testIntervention.getSystem()).isEqualTo(UPDATED_SYSTEM);
        assertThat(testIntervention.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testIntervention.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    public void updateNonExistingIntervention() throws Exception {
        int databaseSizeBeforeUpdate = interventionRepository.findAll().size();

        // Create the Intervention

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInterventionMockMvc.perform(put("/api/interventions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(intervention)))
            .andExpect(status().isCreated());

        // Validate the Intervention in the database
        List<Intervention> interventionList = interventionRepository.findAll();
        assertThat(interventionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteIntervention() throws Exception {
        // Initialize the database
        interventionRepository.save(intervention);
        int databaseSizeBeforeDelete = interventionRepository.findAll().size();

        // Get the intervention
        restInterventionMockMvc.perform(delete("/api/interventions/{id}", intervention.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Intervention> interventionList = interventionRepository.findAll();
        assertThat(interventionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Intervention.class);
    }
}
