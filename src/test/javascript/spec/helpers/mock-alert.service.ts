import { AlertService } from 'ng-jhipster';
import { Sanitizer } from '@angular/core';

export class MockAlertService {

    getMock(sanitizer: Sanitizer) {
        return new AlertService(sanitizer, false);
    }

    init() {}
}
