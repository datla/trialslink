import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit, Sanitizer } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockAlertService } from '../../../helpers/mock-alert.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ConditionDetailComponent } from '../../../../../../main/webapp/app/entities/condition/condition-detail.component';
import { ConditionService } from '../../../../../../main/webapp/app/entities/condition/condition.service';
import { Condition } from '../../../../../../main/webapp/app/entities/condition/condition.model';

describe('Component Tests', () => {

    describe('Condition Management Detail Component', () => {
        let comp:ConditionDetailComponent;
        let fixture:ComponentFixture<ConditionDetailComponent>;
        let service:ConditionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [ConditionDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance:MockBackend, defaultOptions:BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    {
                        provide: AlertService,
                        useFactory: MockAlertService, deps: [Sanitizer]
                    },
                    ConditionService,
                    EventManager
                ]
            }).overrideComponent(ConditionDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConditionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConditionService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                spyOn(service, 'find').and.returnValue(Observable.of(new Condition('aaa')));

                // WHEN
                comp.ngOnInit();

                // THEN
                // we inject conditon - so we can't do onIt tests to verify condition object has been passed
                expect(comp.authorities).toEqual(jasmine.objectContaining(['ROLE_USER', 'ROLE_ADMIN']));
            });
        });
    });

});
