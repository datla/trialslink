import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SecondaryIdDetailComponent } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id-detail.component';
import { SecondaryIdService } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id.service';
import { SecondaryId } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id.model';

describe('Component Tests', () => {

    describe('SecondaryId Management Detail Component', () => {
        let comp: SecondaryIdDetailComponent;
        let fixture: ComponentFixture<SecondaryIdDetailComponent>;
        let service: SecondaryIdService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [SecondaryIdDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    SecondaryIdService
                ]
            }).overrideComponent(SecondaryIdDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SecondaryIdDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SecondaryIdService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new SecondaryId('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.secondaryId).toEqual(jasmine.objectContaining({id:'aaa'}));
            });
        });
    });

});
