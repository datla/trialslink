import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { OutcomeMeasureDetailComponent } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure-detail.component';
import { OutcomeMeasureService } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure.service';
import { OutcomeMeasure } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure.model';

describe('Component Tests', () => {

    describe('OutcomeMeasure Management Detail Component', () => {
        let comp: OutcomeMeasureDetailComponent;
        let fixture: ComponentFixture<OutcomeMeasureDetailComponent>;
        let service: OutcomeMeasureService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [OutcomeMeasureDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    OutcomeMeasureService
                ]
            }).overrideComponent(OutcomeMeasureDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OutcomeMeasureDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OutcomeMeasureService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new OutcomeMeasure('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.outcomeMeasure).toEqual(jasmine.objectContaining({id:'aaa'}));
            });
        });
    });

});
