
  ${AnsiColor.GREEN} _____    _       _     _ _       _
  ${AnsiColor.GREEN}|_   _|  (_)     | |   | (_)     | |
  ${AnsiColor.GREEN}  | |_ __ _  __ _| |___| |_ _ __ | | __
  ${AnsiColor.GREEN}  | | '__| |/ _` | / __| | | '_ \| |/ /
  ${AnsiColor.GREEN}  | | |  | | (_| | \__ \ | | | | |   <
  ${AnsiColor.GREEN}  \_/_|  |_|\__,_|_|___/_|_|_| |_|_|\_\

${AnsiColor.BRIGHT_BLUE}:: Trialslink  :: Running Version ${project.version} ::
:: https://trialslink.com ::${AnsiColor.DEFAULT}
