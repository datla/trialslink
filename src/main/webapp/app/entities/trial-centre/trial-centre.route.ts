import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { TrialCentreComponent } from './trial-centre.component';
import { TrialCentreDetailComponent } from './trial-centre-detail.component';
import { TrialCentrePopupComponent } from './trial-centre-dialog.component';
import { TrialCentreDeletePopupComponent } from './trial-centre-delete-dialog.component';

import { Principal } from '../../shared';


export const trialCentreRoute: Routes = [
  {
    path: 'trial-centre',
    component: TrialCentreComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trialCentre.home.title'
    }
  }, {
    path: 'trial-centre/:id',
    component: TrialCentreDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trialCentre.home.title'
    }
  }
];

export const trialCentrePopupRoute: Routes = [
  {
    path: ':trial-centre-new/:trialId/create',
    component: TrialCentrePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trialCentre.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'trial-centre/:id/edit',
    component: TrialCentrePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trialCentre.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'trial-centre/:id/delete',
    component: TrialCentreDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.trialCentre.home.title'
    },
    outlet: 'popup'
  }
];
