import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { TrialCentre } from './trial-centre.model';
import { TrialCentrePopupService } from './trial-centre-popup.service';
import { TrialCentreService } from './trial-centre.service';

@Component({
    selector: 'jhi-trial-centre-delete-dialog',
    templateUrl: './trial-centre-delete-dialog.component.html'
})
export class TrialCentreDeleteDialogComponent {

    trialCentre: TrialCentre;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private trialCentreService: TrialCentreService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['trialCentre']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.trialCentreService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'trialCentreListModification',
                content: 'Deleted an trialCentre'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trial-centre-delete-popup',
    template: ''
})
export class TrialCentreDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private trialCentrePopupService: TrialCentrePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.trialCentrePopupService
                .open(TrialCentreDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
