import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import * as _ from 'underscore';

import { Concept } from './concept.model';
@Injectable()
export class ConceptService {

    private resourceUrl = 'api/concepts';
    private statusesUrl = 'api/statuses';
    private resourceSearchUrl = 'api/_search/concepts';
    private resourceIndexUrl = 'api/_index/concepts';

    constructor(private http: Http) { }

    create(concept: Concept): Observable<Concept> {
        let copy: Concept = Object.assign({}, concept);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(concept: Concept): Observable<Concept> {
        let copy: Concept = Object.assign({}, concept);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: number): Observable<Concept> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
        ;
    }

    allAsFilter(req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res:any) => this.convertToFilterResponse(res))
        ;
    }

    statusesAsFilter(req?: any): Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(this.statusesUrl, options)
            .map((res:any) => this.convertToFilterResponse(res))
        ;
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    indexAll():Observable<Response> {
        return this.http.get(`${this.resourceIndexUrl}`);
    }

    search(req?:any) {
        let options = this.createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res:any) => this.convertResponse(res))
            ;
    }

    private convertResponse(res:any):any {
        let jsonResponse = res.json();
        let results:Concept[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            results.push(jsonResponse[i]);
        }
        return results;
    }

    private convertToFilterResponse(res:any):any {
        let jsonResponse = res.json();
        let results = [];
        _.each(jsonResponse, function(item:any){
            let o = <any>{};
            o.id = item.id;
            o.text = item.label;
            o.type = item.type;
            results.push(o);
        });
        res._body = results;
        return res;
    }

    private createRequestOption(req?: any): BaseRequestOptions {
        let options: BaseRequestOptions = new BaseRequestOptions();
        if (req) {
            let params: URLSearchParams = new URLSearchParams();
            params.set('page', req.page);
            params.set('size', req.size);
            if (req.sort) {
                params.paramsMap.set('sort', req.sort);
            }
            params.set('query', req.query);

            options.search = params;
        }
        return options;
    }
}
