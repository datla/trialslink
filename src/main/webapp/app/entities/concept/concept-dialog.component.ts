import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { Concept } from './concept.model';
import { ConceptPopupService } from './concept-popup.service';
import { ConceptService } from './concept.service';
@Component({
    selector: 'jhi-concept-dialog',
    templateUrl: './concept-dialog.component.html'
})
export class ConceptDialogComponent implements OnInit {

    concept: Concept;
    authorities: any[];
    isSaving: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private conceptService: ConceptService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['concept']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.concept.id !== undefined) {
            this.conceptService.update(this.concept)
                .subscribe((res: Concept) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.conceptService.create(this.concept)
                .subscribe((res: Concept) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: Concept) {
        this.eventManager.broadcast({ name: 'conceptListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-concept-popup',
    template: ''
})
export class ConceptPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private conceptPopupService: ConceptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.conceptPopupService
                    .open(ConceptDialogComponent, params['id']);
            } else {
                this.modalRef = this.conceptPopupService
                    .open(ConceptDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
