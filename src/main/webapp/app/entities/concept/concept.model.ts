export class Concept {
    constructor(
        public id?: string,
        public label?: string,
        public system?: string,
        public code?: string,
        public type?:string
    ) {
    }
}
