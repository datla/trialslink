import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager, ParseLinks, PaginationUtil, JhiLanguageService, AlertService } from 'ng-jhipster';

import { Trial } from './trial.model';
import { Category } from './category.model';
import { Query } from './query.model';
import { TrialService } from './trial.service';
import { ConceptService } from '../concept/concept.service';
import { OrganisationService } from '../organisation/organisation.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import * as _ from 'underscore';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-trial',
    templateUrl: './trial.component.html'
})
export class TrialComponent implements OnInit, OnDestroy {

    trials: Trial[];
    categories:Category[];
    query: Query;
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    currentSearch: string;
    currentTokens: Array<Object> = [];
    public conditions:Array<string>;
    public locations:Array<string>;
    locationsLookup: any;
    knownStatuses: any;
    private value:any = {};
    lookup: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private trialService: TrialService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private parseLinks: ParseLinks,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private conceptService: ConceptService,
        private organisationService: OrganisationService
    ) {
        this.trials = [];
        this.categories = [];
        this.query = new Query();
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'briefTitle';
        this.reverse = true;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
        this.jhiLanguageService.setLocations(['trial', 'trialStatus', 'studyType', 'trialCentre', 'contact']);
        this.lookup = {ZERO: 'Phase 0', ONE: 'Phase I', TWO: 'Phase II', THREE: 'Phase III',  FOUR: 'Phase IV'};
    }

    selectedCondition(value:any):void {
        this.query.conditions.push(value.id);
        this.search();
    }

    selectedLocation(value:any):void {
        this.query.locations.push(value.id);
        this.search();
    }

    removedCondition(value:any):void {
        this.query.conditions = this.query.conditions.filter(function(v){return v !== value.id});
        this.search();
    }

    removedLocation(value:any):void {
        this.query.locations = this.query.locations.filter(function(v){return v !== value.id});
        this.search();
    }

    omniSearch(value:any):void {
        this.search();
    }

    loadAll () {
        if (this.query.conditions.length > 0 || this.query.locations.length > 0 || this.query.age > 0
                || this.query.genders.length > 0 || this.query.conditionStatuses.length > 0 || this.query.phases.length > 0
                || this.query.statuses.length > 0) {
            this.trialService.search({
                query: this.query,
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: Response) => this.onSuccess(res.json(), res.headers),
                (res: Response) => this.onError(res.json())
            );
            return;
        }
        if (this.currentSearch) {
            this.trialService.omniSearch({
                query: this.currentSearch,
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: Response) => this.onSuccess(res.json(), res.headers),
                (res: Response) => this.onError(res.json())
            );
            return;
        }
        this.trialService.query({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: Response) => this.onSuccess(res.json(), res.headers),
            (res: Response) => this.onError(res.json())
        );
        // load all conditions and locations
        this.conceptService.allAsFilter(
            {
                size: 500
            }
        ).subscribe(
            (res: Response) => {
                this.conditions = res.json().filter(function(c){return c.type !== 'status'});
            },
            (res: Response) => this.onError(res.json())
        );
        // load all statuses
        this.conceptService.statusesAsFilter({
                size: 20
            }
        ).subscribe(
            (res: Response) => {
                this.knownStatuses = _.indexBy(res.json(), 'id');
            },
            (res: Response) => this.onError(res.json())
        );
        this.organisationService.allAsFilter({
                size: 500
            }
        ).subscribe(
            (res: Response) => {
                this.locations = res.json();
                this.locationsLookup = _.indexBy(this.locations, 'id');
            },
            (res: Response) => this.onError(res.json())
        );
    }

    reset () {
        this.page = 0;
        this.trials = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTrials();
    }

    importTrial(id: string) {
        this.trialService.import(id).subscribe(trial => {
            this.router.navigate(['/trial/'+trial.id]);
        });
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId (index: number, item: Trial) {
        return item.id;
    }

    updateFilters(key: any, value: any) {
        // process based on key and if value is present, we remove otherwise we add.
        if(key == 'cstatus'){
            if(_.contains(this.query.conditionStatuses, value)){
                this.query.conditionStatuses = this.query.conditionStatuses.filter(function(v){return v != value;});
            } else {
                this.query.conditionStatuses.push(value);
            }
        } else if(key == 'cstatus'){
            if(_.contains(this.query.conditionStatuses, value)){
                this.query.conditionStatuses = this.query.conditionStatuses.filter(function(v){return v != value;});
            } else {
                this.query.conditionStatuses.push(value);
            }
        } else if(key == 'phase'){
            if(_.contains(this.query.phases, value)){
                this.query.phases = this.query.phases.filter(function(v){return v != value;});
            } else {
                this.query.phases.push(value);
            }
        } else if(key == 'type'){
            if(_.contains(this.query.types, value)){
                this.query.types = this.query.types.filter(function(v){return v != value;});
            } else {
                this.query.types.push(value);
            }
        } else if(key == 'status'){
            if(_.contains(this.query.statuses, value)){
                this.query.statuses = this.query.statuses.filter(function(v){return v != value;});
            } else {
                this.query.statuses.push(value);
            }
        } else if(key == 'sex'){
            if(_.contains(this.query.genders, value)){
                this.query.genders = this.query.genders.filter(function(v){return v != value;});
            } else {
                this.query.genders.push(value);
            }
        } else if(key == 'sites'){
            if(_.contains(this.query.locations, value)){
                this.query.locations = this.query.locations.filter(function(v){return v != value;});
            } else {
                this.query.locations.push(value);
            }
        }

        console.log("this.query  = " , this.query );
        // execute search
        this.search();
    }

    indexAllTrials() {
        this.trialService.indexAllTrials().subscribe(
            (res:Response) => this.alertService.success('Indexed successfully', null, null),
            (res:Response) => this.alertService.error(res.json().message, null, null)
        );
    }

    clear () {
        this.trials = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = 'briefTitle';
        this.reverse = true;
        this.currentSearch = '';
        this.loadAll();
    }

    search () {
        //if (!query) {
        //    return this.clear();
        //}
        this.trials = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        //this.predicate = '_score';
        this.reverse = false;
        //this.currentSearch = query;
        this.loadAll();
    }

    registerChangeInTrials() {
        this.eventSubscriber = this.eventManager.subscribe('trialListModification', (response) => this.reset());
    }

    updatedPredicate(predicate:any) {
        this.trials = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = predicate;
        this.loadAll();
    }

    sortPredicate() {
        this.reverse = !this.reverse;
        this.updatedPredicate(this.predicate);
    }

    sort () {
        if (this.predicate == null) {
            this.predicate = 'briefTitle';
        }
        let result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        return result;
    }

    private onSuccess(data, headers) {

        if((data.results !== undefined && data.results.length > 0) || (data !== undefined && data.length > -1)) {
            this.links = this.processLinks(headers.get('link'));
            this.totalItems = headers.get('X-Total-Count');

            for (let i = 0; i < data.results.length; i++) {
                this.trials.push(data.results[i]);
            }
            // set categories
            let v = [];
            _.mapObject(data.categories, function (value, key) {
                let category = new Category();
                category.key = key;
                category.items = value;
                v.push(category);
            });
            this.categories = v;
        }

        //if ((this.query.conditions.length > 0 || this.query.locations.length > 0 || this.query.age > 0 || this.currentSearch)
        //        && data.results.length > 0) {
        //    for (let i = 0; i < data.results.length; i++) {
        //        this.trials.push(data.results[i]);
        //    }
        //    // set categories
        //    let v = [];
        //    _.mapObject(data.categories, function (value, key) {
        //        let category = new Category();
        //        category.key = key;
        //        category.items = value;
        //        v.push(category);
        //    });
        //    this.categories = v;
        //}
        //else {
        //    for (let i = 0; i < data.length; i++) {
        //        this.trials.push(data[i]);
        //    }
        //}
    }

     // fix for error in ng-hipster where parts are split at ',' instead it should be a location of ',<'
    private processLinks (header) {

        if (header.length === 0) {
            throw new Error('input must not be of zero length');
        }
        // Split parts by comma and < :: FIX for error in ngHipster
        var parts = header.split(',<');
        var links = {};
        // Parse each part into a named link
        parts.forEach(function (p) {
            let section = p.split(';');
            if (section.length !== 2) {
                throw new Error('section could not be split on ";"');
            }
            let url = section[0].replace(/<(.*)>/, '$1').trim();
            let queryString = <any>{};
            url.replace(new RegExp('([^?=&]+)(=([^&]*))?', 'g'), function ($0, $1, $2, $3) { return queryString[$1] = $3; });
            let page = queryString.page;
            if (typeof page === 'string') {
                page = parseInt(page, 10);
            }
            let name = section[1].replace(/rel="(.*)"/, '$1').trim();
            links[name] = page;
        });
        return links;
    }

    private onImportSuccess(data) {
        this.router.navigate(['/trial/'+data.id]);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}
