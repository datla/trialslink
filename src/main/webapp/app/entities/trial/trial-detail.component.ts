import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Rx';

import { EventManager, AlertService, JhiLanguageService  } from 'ng-jhipster';
import { Trial } from './trial.model';
import { TrialCentre } from '../trial-centre';
import { TrialService } from './trial.service';
import { TrialCentreService } from '../trial-centre/trial-centre.service';

@Component({
    selector: 'jhi-trial-detail',
    templateUrl: './trial-detail.component.html'
})
export class TrialDetailComponent implements OnInit, OnDestroy {

    trial: Trial;
    newTrialCentre: TrialCentre;
    private subscription: any;
    eventSubscriber: Subscription;
    private isCreating: boolean;
    @ViewChild('t') t;
    isSaving: boolean;
    isEditing: boolean = false;
    lookup: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private trialService: TrialService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private trialCentreService: TrialCentreService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['trial', 'trialStatus', 'studyType', 'trialCentre', 'contact']);
        this.lookup = {ZERO: 'Phase 0', ONE: 'Phase I', TWO: 'Phase II', THREE: 'Phase III',  FOUR: 'Phase IV'};
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
        this.registerChangeInTrial();
    }

    load (id) {
        this.trialService.find(id).subscribe(trial => {
            this.trial = trial;
            console.log("this.trial  = " , this.trial );
        });
        this.trialCentreService.centresForTrial(id).subscribe(
            (res: Response) => this.onCentresLoad(res.json(), res.headers),
            (res: Response) => this.onError(res.json())
        );
    }

    addNewSite () {
        this.isCreating = !this.isCreating;
         // create and add new trialCetnre to existing sites
        this.newTrialCentre = new TrialCentre();
        console.log("this.newTrialCentre  = " , this.newTrialCentre );
        this.t.select('sites');
    }

    save () {
        this.isSaving = true;
         // save new trial centre
        this.saveTrialCentre(this.newTrialCentre);
    }

    private onAddSuccess (result: TrialCentre) {
        this.eventManager.broadcast({ name: 'trialModification', content: 'OK'});
        this.isSaving = false;
        this.isCreating = false;
        this.trial.sites.unshift(result);
    }

    private saveTrialCentre (trialCentre: TrialCentre) {

        let startDate: Date = new Date();
        startDate.setFullYear(trialCentre.startDate.year, trialCentre.startDate.month - 1, trialCentre.startDate.day);
        console.log("startDate  = " , startDate );
        let endDate: Date = new Date();
        endDate.setFullYear(trialCentre.endDate.year, trialCentre.endDate.month - 1, trialCentre.endDate.day);
        console.log("endDate  = " , endDate );
         // update newTrialCentre
        trialCentre.startDate = startDate.toISOString();
        trialCentre.endDate = endDate.toISOString();
        //trialCentre.trial = this.trial;
        console.log("trialCentre to save/update  = " , trialCentre );

        this.trialService.addCentre(this.trial.id, trialCentre)
            .subscribe((res: TrialCentre) => this.onAddSuccess(res), (res: Response) => this.onSaveError(res.json()));
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }

    trackCentres(index, centre) {
        return centre ? centre.id : undefined;

    }

    trackContacts(index, contact) {
        return contact ? contact.id : undefined;

    }

    registerChangeInTrial() {
        console.log("Recieved trial update event ");
        this.eventSubscriber = this.eventManager.subscribe('trialModification', (response) => this.load(this.trial.id));
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onCentresLoad(data, headers) {
        //this.links = this.parseLinks.parse(headers.get('link'));
        //this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            this.trial.sites.push(data[i]);
        }
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

}
