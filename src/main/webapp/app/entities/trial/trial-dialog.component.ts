import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { Trial } from './trial.model';
import { TrialPopupService } from './trial-popup.service';
import { TrialService } from './trial.service';
@Component({
    selector: 'jhi-trial-dialog',
    templateUrl: './trial-dialog.component.html'
})
export class TrialDialogComponent implements OnInit {

    trial: Trial;
    authorities: any[];
    isSaving: boolean;
    model:any;
    shortNameExists = false;
    phases: Array<any>;
    existingPhases: Array<any>;
    lookup: any;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private trialService: TrialService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['trial', 'trialStatus', 'studyType']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.lookup = {ZERO: 'Phase 0', ONE: 'Phase I', TWO: 'Phase II', THREE: 'Phase III',  FOUR: 'Phase IV'};
        this.phases = [{id: 'ZERO', text: 'Phase 0'}, {id: 'ONE', text: 'Phase I'}, {id: 'TWO', text: 'Phase II'}, {id: 'THREE', text: 'Phase III'},  {id: 'FOUR', text: 'Phase IV'}];
        this.existingPhases = <any>[];
        this.trial.phases.forEach((value) => {
            this.existingPhases.push({id: value, text: this.lookup[value]});
        });
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    selectedPhase(value:any):void {
        this.trial.phases.push(value.id);
    }

    removedPhase(value:any):void {
        this.trial.phases = this.trial.phases.filter(function(v){return v !== value.id});
    }

    save () {
        this.isSaving = true;
        if (this.trial.id !== undefined) {
            this.trialService.update(this.trial)
                .subscribe((res: Trial) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.trialService.create(this.trial)
                .subscribe((res: Trial) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: Trial) {
        this.eventManager.broadcast({ name: 'trialListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
        this.eventManager.broadcast({ name: 'trialModification', content: 'OK'});
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }

    search($event ){
        this.trialService.findByShortName($event)
            .subscribe(response => {
                this.shortNameExists = response.json();
                if(!this.shortNameExists){
                    this.trial.shortName = $event;
                }
            });
    }
}

@Component({
    selector: 'jhi-trial-popup',
    template: ''
})
export class TrialPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private trialPopupService: TrialPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.trialPopupService
                    .open(TrialDialogComponent, params['id']);
            } else {
                this.modalRef = this.trialPopupService
                    .open(TrialDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
