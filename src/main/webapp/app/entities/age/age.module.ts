import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';

import {
    AgeService,
    AgePopupService,
    AgeComponent,
    AgeDetailComponent,
    AgeDialogComponent,
    AgePopupComponent,
    AgeDeletePopupComponent,
    AgeDeleteDialogComponent,
    ageRoute,
    agePopupRoute,
} from './';

let ENTITY_STATES = [
    ...ageRoute,
    ...agePopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AgeComponent,
        AgeDetailComponent,
        AgeDialogComponent,
        AgeDeleteDialogComponent,
        AgePopupComponent,
        AgeDeletePopupComponent,
    ],
    entryComponents: [
        AgeComponent,
        AgeDialogComponent,
        AgePopupComponent,
        AgeDeleteDialogComponent,
        AgeDeletePopupComponent,
    ],
    providers: [
        AgeService,
        AgePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkAgeModule {}
