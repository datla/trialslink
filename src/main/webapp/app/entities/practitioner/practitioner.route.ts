import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { PractitionerComponent } from './practitioner.component';
import { PractitionerDetailComponent } from './practitioner-detail.component';
import { PractitionerPopupComponent } from './practitioner-dialog.component';
import { PractitionerDeletePopupComponent } from './practitioner-delete-dialog.component';

import { Principal } from '../../shared';


export const practitionerRoute: Routes = [
  {
    path: 'practitioner',
    component: PractitionerComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.practitioner.home.title'
    }
  }, {
    path: 'practitioner/:id',
    component: PractitionerDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.practitioner.home.title'
    }
  }
];

export const practitionerPopupRoute: Routes = [
  {
    path: 'practitioner-new',
    component: PractitionerPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.practitioner.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'practitioner/:id/edit',
    component: PractitionerPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.practitioner.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'practitioner/:id/delete',
    component: PractitionerDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.practitioner.home.title'
    },
    outlet: 'popup'
  }
];
