import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { Practitioner } from './practitioner.model';
import { PractitionerService } from './practitioner.service';

@Component({
    selector: 'jhi-practitioner-detail',
    templateUrl: './practitioner-detail.component.html'
})
export class PractitionerDetailComponent implements OnInit, OnDestroy {

    practitioner: Practitioner;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private practitionerService: PractitionerService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['practitioner', 'gender']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.practitionerService.find(id).subscribe(practitioner => {
            this.practitioner = practitioner;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
