export * from './practitioner.model';
export * from './practitioner-popup.service';
export * from './practitioner.service';
export * from './practitioner-dialog.component';
export * from './practitioner-delete-dialog.component';
export * from './practitioner-detail.component';
export * from './practitioner.component';
export * from './practitioner.route';
