import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Condition } from './condition.model';
import { ConditionService } from './condition.service';
@Injectable()
export class ConditionPopupService {
    private isOpen = false;

    constructor(private modalService:NgbModal,
                private router:Router,
                private conditionService:ConditionService) {
    }

    open(component:Component, id?:number | any, createNew?:boolean):NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (!createNew) {
            this.conditionService.find(id).subscribe(condition => {
                this.conditionModalRef(component, condition);
            });
        }
        else {
            let condition = new Condition();
            condition.trialId = id;
            return this.conditionModalRef(component, condition);
        }
    }

    conditionModalRef(component:Component, condition:Condition):NgbModalRef {
        let modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.condition = condition;
        modalRef.result.then(result => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
            this.isOpen = false;
        });
        return modalRef;
    }
}
