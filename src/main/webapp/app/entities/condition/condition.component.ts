import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager, ParseLinks, PaginationUtil, JhiLanguageService, AlertService } from 'ng-jhipster';

import { Condition } from './condition.model';
import { ConditionService } from './condition.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-condition',
    templateUrl: './condition.component.html'
})
export class ConditionComponent implements OnInit, OnDestroy {
    conditions:Condition[];
    currentAccount:any;
    eventSubscriber:Subscription;

    constructor(private jhiLanguageService:JhiLanguageService,
                private conditionService:ConditionService,
                private alertService:AlertService,
                private eventManager:EventManager,
                private principal:Principal) {
        this.jhiLanguageService.setLocations(['condition']);
    }

    loadAll() {
        this.conditionService.query().subscribe(
            (res:Response) => {
                this.conditions = res.json();
            },
            (res:Response) => this.onError(res.json())
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInConditions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index:number, item:Condition) {
        return item.id;
    }


    registerChangeInConditions() {
        this.eventSubscriber = this.eventManager.subscribe('conditionListModification', (response) => this.loadAll());
    }


    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
