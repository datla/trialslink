import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, BaseRequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Condition } from './condition.model';
@Injectable()
export class ConditionService {

    private resourceUrl = 'api/conditions';
    private trialsResourceUrl = 'api/trials';

    constructor(private http:Http) {
    }

    create(condition:Condition):Observable<Condition> {
        let copy:Condition = Object.assign({}, condition);
        if (condition.trialId !== undefined) {
            return this.http.post(`${this.trialsResourceUrl}/${condition.trialId}/conditions`, copy).map((res:Response) => {
                return res.json();
            });
        }
        else {
            return this.http.post(this.resourceUrl, copy).map((res:Response) => {
                return res.json();
            });
        }
    }

    update(condition:Condition):Observable<Condition> {
        let copy:Condition = Object.assign({}, condition);
        return this.http.put(`${this.trialsResourceUrl}/${condition.trialId}/conditions`, copy).map((res:Response) => {
            return res.json();
        });
    }

    find(id:number):Observable<Condition> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res:Response) => {
            return res.json();
        });
    }

    query(req?:any):Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            ;
    }

    conditionsForTrial(id:string, req?:any):Observable<Response> {
        let options = this.createRequestOption(req);
        return this.http.get(`${this.trialsResourceUrl}/${id}/conditions`, options)
            .map((res:any) => this.convertResponse(res, id))
            ;
    }

    delete(trialId: string, id:number):Observable<Response> {
        return this.http.delete(`${this.trialsResourceUrl}/${trialId}/conditions/${id}`);
    }

    deleteAllForTrial(id:string):Observable<Response> {
        return this.http.delete(`${this.trialsResourceUrl}/${id}/conditions`);
    }

    private convertResponse(res:any, id:string):any {
        let jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            jsonResponse[i].trialId = id;
        }
        res._body = jsonResponse;
        return res;
    }

    private createRequestOption(req?:any):BaseRequestOptions {
        let options:BaseRequestOptions = new BaseRequestOptions();
        if (req) {
            let params:URLSearchParams = new URLSearchParams();
            params.set('page', req.page);
            params.set('size', req.size);
            if (req.sort) {
                params.paramsMap.set('sort', req.sort);
            }
            params.set('query', req.query);

            options.search = params;
        }
        return options;
    }
}
