import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { ConditionComponent } from './condition.component';
import { ConditionDetailComponent } from './condition-detail.component';
import { ConditionPopupComponent } from './condition-dialog.component';
import { ConditionDeletePopupComponent } from './condition-delete-dialog.component';

import { Principal } from '../../shared';


export const conditionRoute:Routes = [
    {
        path: 'condition',
        component: ConditionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.condition.home.title'
        }
    }, {
        path: 'condition/:id',
        component: ConditionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.condition.home.title'
        }
    }
];

export const conditionPopupRoute:Routes = [
    {
        path: 'condition-new/:trialId/create',
        component: ConditionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.condition.home.title'
        },
        outlet: 'popup'
    },
    {
        path: 'condition/:id/edit',
        component: ConditionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.condition.home.title'
        },
        outlet: 'popup'
    },
    {
        path: 'condition/:id/delete',
        component: ConditionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.condition.home.title'
        },
        outlet: 'popup'
    }
];
