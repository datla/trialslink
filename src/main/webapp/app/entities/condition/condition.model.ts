export class Condition {
    constructor(public id?:string,
                public label?:string,
                public system?:string,
                public code?:string,
                public status?:string,
                public statusCode?:string,
                public trialId?:string) {
    }
}
