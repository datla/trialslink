export class SecondaryId {
    constructor(
        public id?: string,
        public type?: string,
        public value?: string,
    ) {
    }
}
