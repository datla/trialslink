import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { SecondaryId } from './secondary-id.model';
import { SecondaryIdPopupService } from './secondary-id-popup.service';
import { SecondaryIdService } from './secondary-id.service';
@Component({
    selector: 'jhi-secondary-id-dialog',
    templateUrl: './secondary-id-dialog.component.html'
})
export class SecondaryIdDialogComponent implements OnInit {

    secondaryId: SecondaryId;
    authorities: any[];
    isSaving: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private secondaryIdService: SecondaryIdService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['secondaryId']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.secondaryId.id !== undefined) {
            this.secondaryIdService.update(this.secondaryId)
                .subscribe((res: SecondaryId) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.secondaryIdService.create(this.secondaryId)
                .subscribe((res: SecondaryId) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: SecondaryId) {
        this.eventManager.broadcast({ name: 'secondaryIdListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-secondary-id-popup',
    template: ''
})
export class SecondaryIdPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private secondaryIdPopupService: SecondaryIdPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.secondaryIdPopupService
                    .open(SecondaryIdDialogComponent, params['id']);
            } else {
                this.modalRef = this.secondaryIdPopupService
                    .open(SecondaryIdDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
