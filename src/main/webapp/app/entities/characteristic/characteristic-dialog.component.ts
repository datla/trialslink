import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { Characteristic } from './characteristic.model';
import { CharacteristicPopupService } from './characteristic-popup.service';
import { CharacteristicService } from './characteristic.service';
@Component({
    selector: 'jhi-characteristic-dialog',
    templateUrl: './characteristic-dialog.component.html'
})
export class CharacteristicDialogComponent implements OnInit {

    characteristic: Characteristic;
    authorities: any[];
    isSaving: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private characteristicService: CharacteristicService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['characteristic', 'sex', 'age']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.characteristic.id !== undefined) {
            this.characteristicService.update(this.characteristic)
                .subscribe((res: Characteristic) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.characteristicService.create(this.characteristic)
                .subscribe((res: Characteristic) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: Characteristic) {
        this.eventManager.broadcast({ name: 'characteristicListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-characteristic-popup',
    template: ''
})
export class CharacteristicPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private characteristicPopupService: CharacteristicPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.characteristicPopupService
                    .open(CharacteristicDialogComponent, params['id'], false);
            } else {
                this.modalRef = this.characteristicPopupService
                    .open(CharacteristicDialogComponent, params['trialId'], true);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
