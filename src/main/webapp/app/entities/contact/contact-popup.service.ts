import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Contact } from './contact.model';
import { ContactService } from './contact.service';
@Injectable()
export class ContactPopupService {
    private isOpen = false;
    constructor (
        private modalService: NgbModal,
        private router: Router,
        private contactService: ContactService

    ) {}

    open (component: Component, id?: number | any, createNew?: boolean): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (!createNew) {
            this.contactService.find(id).subscribe(contact => {
                this.contactModalRef(component, contact);
            });
        } else {
            let contact = new Contact();
            contact.trialCentreId = id;
            return this.contactModalRef(component, contact);
        }
    }

    contactModalRef(component: Component, contact: Contact): NgbModalRef {
        let modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.contact = contact;
        modalRef.result.then(result => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
