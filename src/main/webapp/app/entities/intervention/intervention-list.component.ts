import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { EventManager, ParseLinks, PaginationUtil, JhiLanguageService, AlertService } from 'ng-jhipster';

import { Intervention } from './intervention.model';
import { Trial } from '../trial/trial.model';
import { InterventionService } from './intervention.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';

@Component({
    selector: 'jhi-intervention-list',
    templateUrl: './intervention-list.component.html'
})
export class InterventionListComponent implements OnInit, OnDestroy {

    @Input() trial: Trial;
    interventions: Intervention[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private interventionService: InterventionService,
        private alertService: AlertService,
        private eventManager: EventManager,
        private principal: Principal
    ) {
        this.jhiLanguageService.setLocations(['intervention', 'trial', 'trialCentre']);
    }

    loadAll() {
        this.interventionService.interventionsForTrial(this.trial.id).subscribe(
            (res: Response) => {
                this.interventions = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInInterventions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId (index: number, item: Intervention) {
        return item.id;
    }

    registerChangeInInterventions() {
        this.eventSubscriber = this.eventManager.subscribe('interventionListModification', (response) => this.loadAll());
    }


    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}
