import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';

import {
    InterventionService,
    InterventionPopupService,
    InterventionComponent,
    InterventionDetailComponent,
    InterventionDialogComponent,
    InterventionPopupComponent,
    InterventionDeletePopupComponent,
    InterventionDeleteDialogComponent,
    interventionRoute,
    interventionPopupRoute,
} from './';

let ENTITY_STATES = [
    ...interventionRoute,
    ...interventionPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        InterventionComponent,
        InterventionDetailComponent,
        InterventionDialogComponent,
        InterventionDeleteDialogComponent,
        InterventionPopupComponent,
        InterventionDeletePopupComponent,
    ],
    entryComponents: [
        InterventionComponent,
        InterventionDialogComponent,
        InterventionPopupComponent,
        InterventionDeleteDialogComponent,
        InterventionDeletePopupComponent,
    ],
    providers: [
        InterventionService,
        InterventionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkInterventionModule {}
