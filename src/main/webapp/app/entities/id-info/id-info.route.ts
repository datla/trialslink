import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { IdInfoComponent } from './id-info.component';
import { IdInfoDetailComponent } from './id-info-detail.component';
import { IdInfoPopupComponent } from './id-info-dialog.component';
import { IdInfoDeletePopupComponent } from './id-info-delete-dialog.component';

import { Principal } from '../../shared';


export const idInfoRoute: Routes = [
  {
    path: 'id-info',
    component: IdInfoComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.idInfo.home.title'
    }
  }, {
    path: 'id-info/:id',
    component: IdInfoDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.idInfo.home.title'
    }
  }
];

export const idInfoPopupRoute: Routes = [
  {
    path: 'id-info-new',
    component: IdInfoPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.idInfo.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'id-info/:id/edit',
    component: IdInfoPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.idInfo.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'id-info/:id/delete',
    component: IdInfoDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.idInfo.home.title'
    },
    outlet: 'popup'
  }
];
