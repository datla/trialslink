export class IdInfo {
    constructor(
        public id?: string,
        public protocolId?: string,
        public nctId?: string
    ) {
    }
}
