import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { NgbDateMomentParserFormatter } from '../blocks/config/ngb-datepicker-parser-formatter';
import { TruncatePipe } from 'angular2-truncate';

import { CookieService } from 'angular2-cookie/services/cookies.service';
import {
    TrialslinkSharedLibsModule,
    TrialslinkSharedCommonModule,
    CSRFService,
    AuthService,
    AuthServerProvider,
    AccountService,
    InfoService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    Principal,
    HasAnyAuthorityDirective,
    JhiSocialComponent,
    SocialService,
    JhiLoginModalComponent
} from './';

@NgModule({
    imports: [
        TrialslinkSharedLibsModule,
        TrialslinkSharedCommonModule
    ],
    declarations: [
        JhiSocialComponent,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        TruncatePipe
    ],
    providers: [
        CookieService,
        LoginService,
        LoginModalService,
        AccountService,
        InfoService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        SocialService,
        AuthService,
        UserService,
        DatePipe,
        {
            provide: NgbDateParserFormatter,
            useFactory: () => { return new NgbDateMomentParserFormatter("DD-MM-YYYY") }
        }
    ],
    entryComponents: [JhiLoginModalComponent],
    exports: [
        TrialslinkSharedCommonModule,
        JhiSocialComponent,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        DatePipe,
        TruncatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class TrialslinkSharedModule {}
