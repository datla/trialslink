package com.trialslink.domain;

import com.trialslink.domain.enumeration.AgeUnit;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Age.
 */

@Document(collection = "age")
public class Age implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("value")
    private Integer value;

    @NotNull
    @Field("unit")
    @org.springframework.data.elasticsearch.annotations.Field(index = FieldIndex.not_analyzed, type = FieldType.String)
    private AgeUnit unit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getValue() {
        return value;
    }

    public Age value(Integer value) {
        this.value = value;
        return this;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public AgeUnit getUnit() {
        return unit;
    }

    public Age unit(AgeUnit unit) {
        this.unit = unit;
        return this;
    }

    public void setUnit(AgeUnit unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Age age = (Age) o;
        if (age.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, age.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Age{" +
            "id=" + id +
            ", value='" + value + "'" +
            ", unit='" + unit + "'" +
            '}';
    }
}
