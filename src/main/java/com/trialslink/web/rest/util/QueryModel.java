package com.trialslink.web.rest.util;

import java.util.ArrayList;
import java.util.List;

/**
 * A class that represents queries passed to {@link com.trialslink.web.rest.TrialResource}
 */
public class QueryModel {

    List<String> conditions = new ArrayList<>();
    List<String> locations = new ArrayList<>();
    List<String> statuses = new ArrayList<>();
    List<String> phases = new ArrayList<>();
    List<String> types = new ArrayList<>();
    List<String> genders = new ArrayList<>();
    Integer age;

    public QueryModel() {
    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<String> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<String> statuses) {
        this.statuses = statuses;
    }

    public List<String> getPhases() {
        return phases;
    }

    public void setPhases(List<String> phases) {
        this.phases = phases;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public List<String> getGenders() {
        return genders;
    }

    public void setGenders(List<String> genders) {
        this.genders = genders;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("QueryModel{");
        sb.append("conditions=").append(conditions);
        sb.append(", locations=").append(locations);
        sb.append(", statuses=").append(statuses);
        sb.append(", phases=").append(phases);
        sb.append(", types=").append(types);
        sb.append(", age=").append(age);
        sb.append('}');
        return sb.toString();
    }
}
