package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * REST controller for application information.
 */
@RestController
@RequestMapping("/api")
public class AppResource {

    private final Logger log = LoggerFactory.getLogger(AppResource.class);

    private final Environment env;

    public AppResource(Environment env) {
        this.env = env;
    }

    /**
     * GET  /app : get application information
     *
     * @return the ResponseEntity with status 200 (OK) and application version and other information
     */
    @GetMapping("/info")
    @Timed
    public ResponseEntity<Map<String, Object>> getAppInfo() {
        log.debug("REST request to get app");
        Map<String, Object> appInfo = new HashMap<>();
        appInfo.put("name", env.getProperty("spring.application.name"));
        appInfo.put("version", env.getProperty("info.project.version"));
        appInfo.put("buildNumber", env.getProperty("info.project.buildNumber"));
        return ResponseEntity.ok(appInfo);
    }
}
