package com.trialslink.repository;

import com.trialslink.domain.Intervention;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Intervention entity.
 */
@SuppressWarnings("unused")
public interface InterventionRepository extends MongoRepository<Intervention,String> {

    Intervention findOneByLabel(String label);
}
