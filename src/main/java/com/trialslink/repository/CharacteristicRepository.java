package com.trialslink.repository;

import com.trialslink.domain.Characteristic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Characteristic entity.
 */
@SuppressWarnings("unused")
public interface CharacteristicRepository extends MongoRepository<Characteristic,String> {

    Page<Characteristic> findByTrialId(String trialId, Pageable pageable);

    Long deleteByTrialId(String trialId);
}
