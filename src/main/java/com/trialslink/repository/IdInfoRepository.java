package com.trialslink.repository;

import com.trialslink.domain.IdInfo;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the IdInfo entity.
 */
@SuppressWarnings("unused")
public interface IdInfoRepository extends MongoRepository<IdInfo,String> {

}
