package com.trialslink.repository;

import com.trialslink.domain.Trial;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Trial entity.
 */
@SuppressWarnings("unused")
public interface TrialRepository extends MongoRepository<Trial,String> {
    Trial findOneByShortName(String shortName);
}
