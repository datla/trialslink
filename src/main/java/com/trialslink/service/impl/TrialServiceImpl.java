package com.trialslink.service.impl;

import com.trialslink.domain.Organisation;
import com.trialslink.domain.Trial;
import com.trialslink.repository.TrialRepository;
import com.trialslink.search.TrialSearchRepository;
import com.trialslink.service.TrialService;
import com.trialslink.service.util.LookupGenerator;
import com.trialslink.web.rest.util.QueryModel;
import org.elasticsearch.index.IndexNotFoundException;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

/**
 * Service Implementation for managing Trial.
 */
@Service
public class TrialServiceImpl implements TrialService{

    private final Logger log = LoggerFactory.getLogger(TrialServiceImpl.class);
    
    private final TrialRepository trialRepository;
    private final TrialSearchRepository trialSearchRepository;
    private final ElasticsearchTemplate elasticsearchTemplate;
    private final LookupGenerator lookupGenerator;

    public TrialServiceImpl(TrialRepository trialRepository, TrialSearchRepository trialSearchRepository,
                            ElasticsearchTemplate elasticsearchTemplate,
                            LookupGenerator lookupGenerator) {
        this.trialRepository = trialRepository;
        this.trialSearchRepository = trialSearchRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.lookupGenerator = lookupGenerator;
    }

    /**
     * Save a trial.
     *
     * @param trial the entity to save
     * @return the persisted entity
     */
    @Override
    public Trial save(Trial trial) {
        log.debug("Request to save Trial : {}", trial);
        Trial result = trialRepository.save(trial);
        trialSearchRepository.delete(result);
        trialSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the trials.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<Trial> findAll(Pageable pageable) {
        log.debug("Request to get all Trials");
        Page<Trial> result = trialRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one trial by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public Trial findOne(String id) {
        log.debug("Request to get Trial : {}", id);
        Trial trial = trialRepository.findOne(id);
        return trial;
    }

    /**
     *  Get one trial by shortName.
     *
     *  @param shortName the shortName of the entity
     *  @return the entity
     */
    @Override
    public Trial findOneByShortName(String shortName) {
        log.debug("Request to get Trial : {}", shortName);
        Trial trial = trialRepository.findOneByShortName(shortName);
        return trial;
    }

    /**
     *  Delete the  trial by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Trial : {}", id);
        trialRepository.delete(id);
        trialSearchRepository.delete(id);
    }

    /**
     *  Index all the trials.
     *
     *  @return the boolean that represents the success of the index action
     */
    @Override
    public boolean indexAll() {
        log.debug("Request to get all Trials");
        boolean result = false;

        // delete existing indices
        try {
            elasticsearchTemplate.deleteIndex(Trial.class);
            elasticsearchTemplate.deleteIndex(Organisation.class);
        } catch (IndexNotFoundException e) {
            log.error("Error deleting indices. Assuming index does not exist.");
        }

        try {
            trialRepository.findAll().forEach(trial -> trial.getSites().forEach(site -> {
                trialSearchRepository.save(trial);
            }));
            result = true;
        } catch (Exception e) {
            log.error("Error bulk indexing trials.Nested exception is : ", e);
        }

        return result;
    }

    /**
     * Search for the trial corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public FacetedPage<Trial> search(QueryModel query, Pageable pageable) {
        log.debug("Request to search for a page of Trials for query {}", query);

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        BoolQueryBuilder conceptTypesQueryBuilder = QueryBuilders.boolQuery();
        // expand query to include both ancestors and descendants
        Set<String> expandedIds = new HashSet<>();
        for (String condition : query.getConditions()) {
            expandedIds.addAll(lookupGenerator.getAncestors(condition));
            expandedIds.addAll(lookupGenerator.getDescendants(condition));
            // now also add self
            expandedIds.add(condition);
        }

        for(String condition : expandedIds) {
            conceptTypesQueryBuilder.should(QueryBuilders.matchQuery("conditions.code", condition));
        }

        BoolQueryBuilder sourcesQueryBuilder = QueryBuilders.boolQuery();
        for(String location : query.getLocations()) {
            sourcesQueryBuilder.should(QueryBuilders.matchQuery("sites.orgId", location));
        }

        BoolQueryBuilder statusQueryBuilder = QueryBuilders.boolQuery();
        for(String status : query.getStatuses()) {
            statusQueryBuilder.should(QueryBuilders.matchQuery("status", status));
        }

        BoolQueryBuilder phaseQueryBuilder = QueryBuilders.boolQuery();
        for(String phase : query.getPhases()) {
            phaseQueryBuilder.should(QueryBuilders.matchQuery("phases", phase));
        }

        BoolQueryBuilder typeQueryBuilder = QueryBuilders.boolQuery();
        for(String type : query.getTypes()) {
            typeQueryBuilder.should(QueryBuilders.matchQuery("type", type));
        }

        BoolQueryBuilder genderQueryBuilder = QueryBuilders.boolQuery();
        for(String gender : query.getGenders()) {
            genderQueryBuilder.should(QueryBuilders.matchQuery("eligibilities.sex", gender));
        }

        if(query.getAge() != null) {
            boolQueryBuilder.must(QueryBuilders.rangeQuery("eligibilities.minAge.value").lte(query.getAge()));
            boolQueryBuilder.must(QueryBuilders.rangeQuery("eligibilities.maxAge.value").gte(query.getAge()));
        }

        // we only add conditions clause if there are more than 1 condition specified
        if(query.getConditions().size() > 0){
            boolQueryBuilder.must(conceptTypesQueryBuilder);
        }

        // we only add locations clause if there are more than 1 locations specified
        if(query.getLocations().size() > 0){
            boolQueryBuilder.must(sourcesQueryBuilder);
        }

        // we only add phases clause if there are more than 1 phase specified
        if (query.getPhases().size() > 0){
            boolQueryBuilder.must(phaseQueryBuilder);
        }

        // we only add status clause if there are more than 1 status specified
        if(query.getStatuses().size() > 0){
            boolQueryBuilder.must(statusQueryBuilder);
        }

        // we only add types clause if there are more than 1 type specified
        if(query.getTypes().size() > 0){
            boolQueryBuilder.must(typeQueryBuilder);
        }

        // we only add gender clause if there are more than 1 gender specified
        if (query.getGenders().size() > 0){
            boolQueryBuilder.must(genderQueryBuilder);
        }

        log.debug("boolQueryBuilder = " + boolQueryBuilder);
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder)
                .addAggregation(new TermsBuilder("type").field("type").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("status").field("status").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("phase").field("phases").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("sites").field("sites.orgId").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("sex").field("eligibilities.sex").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("cstatus").field("conditions.statusCode").size(5).order(Terms.Order.term(true)))
                .build();

        FacetedPage<Trial> page = elasticsearchTemplate.queryForPage(searchQuery, Trial.class);

        return page;
    }

    /**
     * Omni search for the trial corresponding to the query but not on conditions or sites.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public FacetedPage<Trial> omniSearch(String query, Pageable pageable) {
        log.debug("Request to search for a page of Trials for query {}", query);

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(QueryBuilders.multiMatchQuery(query, "idInfo.*", "shortName", "keywords", "title", "id"));

        log.debug("boolQueryBuilder = " + boolQueryBuilder);
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder)
                .addAggregation(new TermsBuilder("type").field("type").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("status").field("status").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("phase").field("phases").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("sites").field("sites.orgId").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("sex").field("eligibilities.sex").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("cstatus").field("conditions.statusCode").size(5).order(Terms.Order.term(true)))
                .build();

        FacetedPage<Trial> page = elasticsearchTemplate.queryForPage(searchQuery, Trial.class);

        return page;
    }

    /**
     * Return all trials corresponding to the page including all categories.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public FacetedPage<Trial> findAllWithCategories(Pageable pageable) {
        log.debug("Request to search for a page of Trials for page {}", pageable);
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.matchAllQuery())
                .addAggregation(new TermsBuilder("type").field("type").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("status").field("status").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("phase").field("phases").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("sites").field("sites.orgId").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("sex").field("eligibilities.sex").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("cstatus").field("conditions.statusCode").size(5).order(Terms.Order.term(true)))
                .build();

        FacetedPage<Trial> page = elasticsearchTemplate.queryForPage(searchQuery, Trial.class);

        return page;
    }
}
