package com.trialslink.service;

import com.trialslink.domain.*;
import com.trialslink.repository.ConceptRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * A concrete implementation of {@link com.trialslink.domain.Visitor} that visits {@link com.trialslink.domain.Trial}s.
 */
@Service
public class TrialVisitorService {

    private final ConceptRepository conceptRepository;
    private final ConditionService conditionService;
    private final CharacteristicService characteristicService;
    private final TrialService trialService;
    private final TrialCentreService trialCentreService;
    private final Logger log = LoggerFactory.getLogger(TrialVisitorService.class);

    public TrialVisitorService(ConceptRepository conceptRepository, ConditionService conditionService,
                               TrialService trialService, TrialCentreService trialCentreService,
                               CharacteristicService characteristicService) {
        this.conceptRepository = conceptRepository;
        this.conditionService = conditionService;
        this.trialService = trialService;
        this.trialCentreService = trialCentreService;
        this.characteristicService = characteristicService;
    }

    public Trial visit(Trial trial) {
        Set<Condition> conditions = new HashSet<>();
        Set<String> oldConditionsIds = new HashSet<>();
        // re attach conditions
        for (Condition condition : trial.getConditions()) {
            Concept c = conceptRepository.findOneByIdOrLabel(condition.getCode(), condition.getLabel());
            if(c != null) {
                log.info("Found match for id {} or label {} ", condition.getCode(), condition.getLabel());
                Condition con = new Condition().code(c.getId()).label(c.getLabel()).system(condition.getSystem());
                // get associated status
                Concept status = conceptRepository.findOneByIdOrLabel(condition.getStatusCode(), condition.getStatus());
                if(status != null) {
                    con = con.statusCode(condition.getStatusCode())
                            .status(condition.getStatus());
                }
                con.setTrialId(trial.getId());
                con = conditionService.save(con);
                conditions.add(con);
            } else {
                log.error("No match found for id {} or label {} ", condition.getCode(), condition.getLabel());
            }
            // collect existing id for deletion
            oldConditionsIds.add(condition.getId());
        }
        // now replace all existing conditions with new conditions set
        trial.setConditions(conditions);

        Set<TrialCentre> sites = new HashSet<>();
        Set<String> oldSiteIds = new HashSet<>();
        // re attach conditions
        for (TrialCentre site : trial.getSites()) {
            TrialCentre t = trialCentreService.findOne(site.getId());
            if(t == null) {
                log.info("Not found match for id {}. Will try to reattach ", site.getId());
                // collect existing id for deletion
                oldSiteIds.add(site.getId());
                site.setTrialId(trial.getId());
                site.setId(null);
                site = trialCentreService.save(site);
                sites.add(site);
            } else {
                log.error("Match found for id {}. Assuming site [] is already attached to trial ", site.getId(), site.getName());
            }
        }

        Set<Characteristic> eligibilities = new HashSet<>();
        Set<String> oldEligbilityIds = new HashSet<>();
        // re attach eligibilities
        for (Characteristic characteristic : trial.getEligibilities()) {
            Characteristic c = characteristicService.findOne(characteristic.getId());
            if(c == null) {
                log.info("Not found match for id {}. Will try to reattach ", characteristic.getId());
                // collect existing id for deletion
                oldEligbilityIds.add(characteristic.getId());
                characteristic.setTrialId(trial.getId());
                characteristic.setId(null);
                characteristic = characteristicService.save(characteristic);
                eligibilities.add(characteristic);
            } else {
                log.error("Match found for id {}. Assuming characteristic is already attached to trial ", characteristic.getId());
            }
        }
        // now replace all existing conditions with new conditions set
        trial.setConditions(conditions);
        trial.setSites(sites);
        trial.setEligibilities(eligibilities);

        // now save trial
        trial = trialService.save(trial);
        log.info("Saved trial {} after updating conditions {}", trial.getId(), conditions);
        // delete old conditions
        oldConditionsIds.forEach(conditionService::delete);
        // delete old sites
        oldSiteIds.forEach(trialCentreService::delete);
        // delete old eligibilities
        oldEligbilityIds.forEach(characteristicService::delete);


        return trial;
    }
}
